#!/usr/bin/python
"""
Speedy Loop programming problem for Truewhitespace
"""
from scipy.sparse import csr_matrix
from scipy.sparse import csgraph
from itertools import tee
from functools import reduce
import logging
import os


def read_input(filename: str = 'input.txt') -> (list, csr_matrix):
    """
    Read in the input file, return it as a list and a csr_matrix representation.

    Each line of input represents an edge, with a pair of letters indicating the nodes
    and a number indicating the distance. The node letters are converted to array
    indices (A=0, B=1, C=2, etc.) and placed into three arrays for row index,
    col index, and distance value, that are then used to construct a scipy csr_matrix.

    :param filename: name of the file with the input data
    :return: distance matrix as a list of rows (lists), distance matrix as a csr_matrix
   """

    row_indices = []
    col_indices = []
    distances = []
    with open(filename) as f:
        for one_line in f:
            row_indices.append(ord(one_line[0]) - ord('A'))
            col_indices.append(ord(one_line[1]) - ord('A'))
            distances.append(one_line[2])

    cm: csr_matrix = csr_matrix(
        (distances,
         (row_indices, col_indices)
         ),
        dtype=int,
        shape=(5, 5)
    )

    lm = cm.toarray().tolist()

    return lm, cm


class NoRouteException(Exception):
    """
    Thrown if there is no route between two nodes
    """

    def __init__(self, node1: str, node2: str) -> None:
        self.node1 = node1
        self.node2 = node2
        self.message = f"There is no route from {node1} and {node2}"


def dist(x: str, y: str, m: csr_matrix) -> int:
    """
    Distance from x to y, if there is a one-hop route.

    If there is no direct route, throw a NoRouteException

    :param x: start node
    :param y: destination node
    :param m: array of distances between nodes
    :return: distance of the edge between the two nodes
    """
    if (result := m[ord(x) - ord('A'), ord(y) - ord('A')]) == 0:
        raise NoRouteException(x, y)
    else:
        return result


def dist_multinode(route: str, m: csr_matrix) -> str:
    """
    Sums distance along a path of edges

    Uses the itertools tee() formula to grab successive pairs of nodes
    :param route: string with list of nodes
    :param m: array of distances between nodes
    :return: total distance along the route, or an error message
    """
    try:
        a, b = tee(route)
        next(b, None)
        return str(reduce(lambda s, p: s + dist(p[0], p[1], m), zip(a, b), 0))
    except NoRouteException as e:
        logging.getLogger().warning(e.message)
        return "NO SUCH ROUTE"


def dfs_traverse(current_node: int,
                 dist_matrix: list,
                 current_depth: int = 0,
                 current_dist: int = 0,
                 stop_node: int = None,
                 stop_depth: int = 20,  # failsafe to prevent infinite recursion
                 stop_dist=None,
                 dest_node=None
                 ) -> list:
    """
    Depth first traversal of the graph. Limit depth by:
        - Reaching stop node
        - Reaching max depth
        - Reaching max distance

    If otherwise unspecified, maximum depth is failsafed to 20 to prevent infinite
    recurson. dest_node is used to record paths that end at a specific node, but
    where we are interested in paths that are longer because they go through a cycle.

    :param current_node: the node we are currently examining
    :param dist_matrix: the matrix of distances as a list of rows (lists)
    :param current_depth: the current depth of our recursion
    :param current_dist: the current distance we have traversed so far
    :param stop_node: the target node of our path(s)
    :param stop_depth: the maximum depth of recursion
    :param stop_dist: the maximum distance of a path
    :param dest_node: the node that we are interested in, but which may not
                        end the path exploration due to cycles
    :return: List of tuples [("ABCD", 12),
             ("AEC", 7),
             ...]
    """
    result = []

    for next_node, next_node_dist in enumerate(dist_matrix[current_node]):
        # check each potential destination node
        if next_node_dist != 0:
            # edge exists
            if next_node == stop_node:
                # edge goes to stop node, add this node + stop node
                result += [(chr(stop_node + 65), next_node_dist)]
                continue

            if stop_depth is not None:
                if current_depth + 1 == stop_depth:
                    if stop_node is None:
                        # no stop node, so stopping at this node on max depth
                        # if there is a stop node then stop_depth is a search limit, so no result
                        result += [(chr(next_node + 65), 0)]
                    continue

            if stop_dist is not None:
                if current_dist + next_node_dist >= stop_dist:
                    if stop_node is None:
                        # no stop node, so stopping at this node due to exceeding max distance
                        # if there is a stop node then stop_dist is a search limit, so no result
                        result += [('', 0)]
                    continue

            if next_node == dest_node:
                # we want to note that we hit a path to the dest_node, but further paths
                # may exist due to cycles, so don't use continue statement
                result += [(chr(dest_node + 65), next_node_dist)]

            # edge exists and no stop conditions are met, so recurse on it
            recursion_result: list = dfs_traverse(next_node,
                                                  dist_matrix,
                                                  current_depth=current_depth + 1,
                                                  current_dist=current_dist + next_node_dist,
                                                  stop_node=stop_node,
                                                  stop_depth=stop_depth,
                                                  stop_dist=stop_dist,
                                                  dest_node=dest_node
                                                  )

            # if something came back, prepend the current node to each path,
            # then append the list to the result
            result += [(pd[0], pd[1] + next_node_dist) for pd in recursion_result]

    # unique the list
    uniqued_result = list(set(result))

    # prepend the current node to each string in the list
    return [(chr(current_node + 65) + pd[0], pd[1]) for pd in uniqued_result]


def example_1(dist_csr_matrix: csr_matrix) -> str:
    result = dist_multinode('ABC', dist_csr_matrix)
    logging.getLogger().info(f"1. Distance for route A-B-C = {result}")
    return result


def example_2(dist_csr_matrix: csr_matrix) -> str:
    result = dist_multinode('AD', dist_csr_matrix)
    logging.getLogger().info(f"2. Distance for route A-D = {result}")
    return result


def example_3(dist_csr_matrix: csr_matrix) -> str:
    result = dist_multinode('ADC', dist_csr_matrix)
    logging.getLogger().info(f"3. Distance for route A-D-C = {result}")
    return result


def example_4(dist_csr_matrix: csr_matrix) -> str:
    result = dist_multinode('AEBCD', dist_csr_matrix)
    logging.getLogger().info(f"4. Distance for route A-E-B-C-D = {result}")
    return result


def example_5(dist_csr_matrix: csr_matrix) -> str:
    result = dist_multinode('AED', dist_csr_matrix)
    logging.getLogger().info(f"5. Distance for route A-E-D = {result}")
    return result


def example_6(dist_matrix: list) -> str:
    result = dfs_traverse(2, dist_matrix, stop_depth=3, stop_node=2)
    logging.getLogger().info(f"6. Cycles starting from C with a maximum of three stops = {result}")
    return str(len(result))


def example_7(dist_matrix: list) -> str:
    result = [path_pair for path_pair in dfs_traverse(0, dist_matrix, stop_depth=4)
              if (path_pair[0][0] == 'A') & (path_pair[0][-1] == 'C')]
    logging.getLogger().info(f"7. Paths from A to C with exactly four stops = {result}")
    return str(len(result))


def example_8(dist_csr_matrix: csr_matrix) -> str:
    result = int(csgraph.shortest_path(dist_csr_matrix)[0][2])
    logging.getLogger().info(f"8. Shortest distance from A to C = {result}")
    return str(result)


def example_9(dist_matrix: list) -> str:
    result = min([n[1] for n in dfs_traverse(1, dist_matrix, stop_depth=10, stop_node=1)])
    logging.getLogger().info(f"9. Shortest distance for a cycle from B to B = {result}")
    return str(result)


def example_10(dist_matrix: list) -> str:
    result = [path_pair for path_pair in dfs_traverse(2, dist_matrix, stop_dist=30, dest_node=2)
              if (path_pair[0][0] == 'C') & (path_pair[0][-1] == 'C')]
    logging.getLogger().info(f"10. Routes from C to C with a distance of less than 30 = {result}")
    return str(len(result))


if __name__ == '__main__':
    g_dist_matrix, g_dist_csr_matrix = read_input()

    log_level = os.environ.get("LOG_LEVEL")
    if log_level is not None:
        logging.basicConfig(level=logging.getLevelName(log_level))

    print(example_1(g_dist_csr_matrix))
    print(example_2(g_dist_csr_matrix))
    print(example_3(g_dist_csr_matrix))
    print(example_4(g_dist_csr_matrix))
    print(example_5(g_dist_csr_matrix))
    print(example_6(g_dist_matrix))
    print(example_7(g_dist_matrix))
    print(example_8(g_dist_csr_matrix))
    print(example_9(g_dist_matrix))
    print(example_10(g_dist_matrix))
