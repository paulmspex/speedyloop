"""
pytest functions for each of the ten cases for the Speedy Loop problem.
"""

import pytest
import main
import logging
import os


@pytest.fixture
def dist_matrix():
    m1, m2 = main.read_input()
    yield m1


@pytest.fixture
def dist_csr_matrix():
    m1, m2 = main.read_input()
    yield m2


def test_example_1(dist_csr_matrix, caplog):
    log_level = os.environ.get("LOG_LEVEL")
    if log_level is not None:
        caplog.set_level(logging.getLevelName(log_level))

    assert main.example_1(dist_csr_matrix) == "9"
    print(caplog.text)


def test_example_2(dist_csr_matrix, caplog):
    log_level = os.environ.get("LOG_LEVEL")
    if log_level is not None:
        caplog.set_level(logging.getLevelName(log_level))

    assert main.example_2(dist_csr_matrix) == "5"
    print(caplog.text)


def test_example_3(dist_csr_matrix, caplog):
    log_level = os.environ.get("LOG_LEVEL")
    if log_level is not None:
        caplog.set_level(logging.getLevelName(log_level))

    assert main.example_3(dist_csr_matrix) == "13"
    print(caplog.text)


def test_example_4(dist_csr_matrix, caplog):
    log_level = os.environ.get("LOG_LEVEL")
    if log_level is not None:
        caplog.set_level(logging.getLevelName(log_level))
    assert main.example_4(dist_csr_matrix) == "22"
    print(caplog.text)


def test_example_5(dist_csr_matrix, caplog):
    log_level = os.environ.get("LOG_LEVEL")
    if log_level is not None:
        caplog.set_level(logging.getLevelName(log_level))
    assert main.example_5(dist_csr_matrix) == "NO SUCH ROUTE"
    print(caplog.text)


def test_example_6(dist_matrix, caplog):
    log_level = os.environ.get("LOG_LEVEL")
    if log_level is not None:
        caplog.set_level(logging.getLevelName(log_level))
    assert main.example_6(dist_matrix) == "2"
    print(caplog.text)


def test_example_7(dist_matrix, caplog):
    log_level = os.environ.get("LOG_LEVEL")
    if log_level is not None:
        caplog.set_level(logging.getLevelName(log_level))
    assert main.example_7(dist_matrix) == "3"
    print(caplog.text)


def test_example_8(dist_csr_matrix, caplog):
    log_level = os.environ.get("LOG_LEVEL")
    if log_level is not None:
        caplog.set_level(logging.getLevelName(log_level))
    assert main.example_8(dist_csr_matrix) == "9"
    print(caplog.text)


def test_example_9(dist_matrix, caplog):
    log_level = os.environ.get("LOG_LEVEL")
    if log_level is not None:
        caplog.set_level(logging.getLevelName(log_level))
    assert main.example_9(dist_matrix) == "9"
    print(caplog.text)


def test_example_10(dist_matrix, caplog):
    log_level = os.environ.get("LOG_LEVEL")
    if log_level is not None:
        caplog.set_level(logging.getLevelName(log_level))
    assert main.example_10(dist_matrix) == "7"
    print(caplog.text)
