## PROBLEM ONE: SPEEDY LOOP

This was an interesting exercise in recursive graph traversal. 
The `scipy.sparse` package provides some interesting functions. 
I ended up only using one of them, but even then it was useful 
to have the data structures as a thinking framework. 

The problem is not inherently an object-oriented problem, so 
I decided to keep it as just a collection of functions, rather
that defining classes.  

Python version 3.9.6 was used in the development, but any Python
version 3.8 or above should work. The necessary modules are 
listed in the standard `requirements.txt` file. 

Running the `main.py` file will produce the desired output to 
stdout. However, it will also log a WARNING level log output
to stderr for example 5, since there is no route from E to D. 
I made this decision since the problem description called for
production-quality code, and in a production environment I 
would definitely want to see exceptions logged to stderr. 
The basic run command is: 

`python main.py`

You can eliminate the warning by sending stderr elsewhere. 

`python main.py 2> /dev/null`

Or you can eliminate the warning by setting the environment
variable `LOG_LEVEL` to `ERROR` or `CRITICAL`. 

`LOG_LEVEL=ERROR python main.py`

Conversely, you can get more detail by setting the log level
to `INFO`: 

`LOG_LEVEL=INFO python main.py` 

### pytest

The pytest framework is also set up to run the ten tests, and
uses assert to pass or fail the results. 

`pytest -v tests/tests.py`

To see log messages, use: 

`pytest -v --log-cli-level=WARNING tests/tests.py`

or

`pytest -v --log-cli-level=INFO tests/tests.py`
